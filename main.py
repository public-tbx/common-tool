import os

INSTRODUCE='''\
Welcome to tienbx!
This is a common tool for all projects.\
'''

print(INSTRODUCE)
## Print and fill our choose
## platform map type:
platform = {
    "Docker": ["Install Docker", "Install docker compose"],
    "AWS": ["Install AWS CLI", "Install AWS SDK"],
    "K8s": ["Install K8s", "Install K8s Dashboard"]
}
install_docker='''\
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin\
'''

## Choose platform
for index, value in enumerate(platform):
    print(f'{index+1}. {value}')
choosePlatform = int(input("Choose your platform (1 or 2 ..): "))

## Choose tool
print("Please chosse your tool:")
for index, value in enumerate(platform[list(platform.keys())[choosePlatform-1]]):
    print(f'{index+1}. {value}')
chooseTool = int(input("Choose your tool (1 or 2 ..): "))
chooseToolName = platform[list(platform.keys())[choosePlatform-1]][chooseTool-1].replace(" ", "_").lower()
print(chooseToolName)

## Install tool by command ubuntu
def installTool(toolName):
    print(f'Installing {toolName}...')
    cmd=globals()[toolName]
    os.system(f'{cmd}')
installTool(chooseToolName)